Ext.define('TodoList.controller.TodoController', {
    extend: 'Ext.app.Controller',
	requires : ['Ext.MessageBox'],
    config: {
		stores: ['TodoStore'],
        refs: {
			listView: '#todoList',
			editView: '#todoEdit',
			
			listEdit: '#todoList button#edit',
            listDelete: '#todoList button#delete',
			editSubmitButton: '#todoEdit button',
			
			editForm: '#todoEdit formpanel'
        },
		control: {
                listDelete: {
                    tap: 'onTodoDelete'
                },
				listEdit: {
                    tap: 'onTodoEdit'
                },
				editSubmitButton: {
					tap: 'onFormSubmit'
				}
        }
    },

	
	onTodoDelete: function(field) {
		var id = field.up().down('#id').getValue();
		if (id != -1) {
			var store = Ext.getStore('todoStore');
			var object = store.getById(id);
			
			store.remove(object);
			
			Ext.Msg.alert('Value Changed', object.getData().description + ' --> Deleted');
		}
	},
	onTodoEdit: function(field) {
		var id = field.up().down('#id').getValue();
		if (id != -1) {
			var store = Ext.getStore('todoStore');
			var object = store.getById(id);
			
			this.getEditForm().setRecord(object);
			Ext.getCmp('mainPanel').setActiveItem(this.getEditView());
		}
	},
	onFormSubmit: function(form) {
		var form = this.getEditForm();
		var record = Ext.create('TodoList.model.TodoModel', form.getValues()); 
		var recordData = record.getData();
		
		if (recordData.description != "") {
			var store = Ext.getStore('todoStore');
			var object = store.getById(recordData.id);
			
			if (object != null) {
				var objectData = object.getData();
				objectData.done = recordData.done;
				objectData.description = recordData.description;
				object.save();
				Ext.Msg.alert('Ok','Object has been updated');
			} else {
				record.save();
				Ext.Msg.alert('Ok','Object has been added');
			}
			//Force store to reload from localstorage
			store.load();
			
			// Clear form values
			form.reset();
			Ext.getCmp('mainPanel').setActiveItem(this.getListView());
		} else {
			Ext.Msg.alert('Error','Description cannot be empty');
		}
	}
});