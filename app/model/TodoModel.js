Ext.define('TodoList.model.TodoModel', {
    extend: 'Ext.data.Model',
    config: {
		idProperty: 'id',
        fields: [
            { name: 'id', type: 'int' },
            { name: 'description', type: 'string' },
			{ name: 'done', type: 'boolean' },
        ],
		proxy: {
            type: 'localstorage',
            id  : 'Todos'
        }
    }
});