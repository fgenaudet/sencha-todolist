Ext.define('TodoList.store.TodoStore', {
   extend: 'Ext.data.Store',
   config:{
	   model: 'TodoList.model.TodoModel',
	   storeId: 'todoStore',
	   autoLoad: true,
       autoSync: true
	}
});