Ext.define('TodoList.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    config: {
        tabBarPosition: 'bottom',
        items: [
            Ext.create('TodoList.view.TodoListView', {
						title: 'Todo list',
						id: 'todoList',
						iconCls: 'home',
						store: 'todoStore'
			}),
             Ext.create('TodoList.view.TodoEditView', {
						title: 'Todo edit',
						id: 'todoEdit',
						iconCls: 'star',
			})
        ]
    }
});

