Ext.define("TodoList.view.TodoEditView", {
		extend: 'Ext.tab.Panel',
		config : {
			items: [
				{
					title: 'Add/Edit',
					xtype: 'formpanel',
					layout: 'vbox',
					
					items: [
						{
							xtype: 'fieldset',
							title: 'Add/Edit a Todo',
							items: [
								{
									xtype: 'textfield',
									label: 'Description',
									name: 'description',
									id: 'description',
								},
								{
									xtype: 'checkboxfield',
									label: 'Done',
									name: 'done',
									id: 'done',
								},
								{
									xtype: 'hiddenfield',
									name: 'id',
									id: 'id',
								},
							]
						},
						{
							xtype: 'button',
							id: 'submitButton',
							text: 'Send',
							ui: 'confirm'
						}
					]
				}
			]
		}
	}
);


