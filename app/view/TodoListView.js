Ext.define('TodoList.view.TodoListView', {
    extend: 'Ext.List',
	
    config: {
		defaultType: 'todolistitem',
        useComponents: true,
		title: 'Todos',
		emptyText: 'No todo found yet!'
    }
});

Ext.define('MyDataItem', {
    extend: 'Ext.dataview.component.ListItem',
    alias: 'widget.todolistitem',

    config: {
        layout: {
            type: 'hbox'
        },
        defaults: {
            margin: 5
        },
        items: [
		{
            xtype: 'component',
            html: 'val2',
			flex: 4,
			style: 'text-align: center',
			itemId: 'description',
			cls: 'static-text'
        },{
            xtype: 'checkboxfield',
            checked: false,
			itemId: 'done',
			style: 'background-color: transparent',
			disabled: true
        },{
			xtype: 'button',
			itemId: 'delete',
			text: 'Delete',
			cls: 'delete-button'
		},{
			xtype: 'button',
			itemId: 'edit',
			text: 'Edit',
			cls: 'edit-button'
		},{
			xtype: 'hiddenfield',
			value: '-1',
			itemId: 'id'
		}]
    },
    updateRecord: function(record) {
		if (record != undefined) {
			var me = this;

			me.down('#description').setHtml(record.get('description'));
			me.down('#done').setChecked(record.get('done'));
			me.down('#id').setValue(record.get('id'));
		
			me.callParent(arguments);
		}
    }
});