Ext.application({
    name: 'TodoList',
    views: ['TodoListView', 'TodoEditView'],
	stores : ['TodoStore'],
	models : ['TodoModel'],
	controllers : ['TodoController'],

    launch: function() {
        var store = Ext.create('TodoList.store.TodoStore');
			
		var listpanel = Ext.create('TodoList.view.TodoListView', {
					title: 'Todo list',
					id: 'todoList',
					iconCls: 'home',
					store: store
		});
		var editpanel = Ext.create('TodoList.view.TodoEditView', {
					title: 'Create a todo',
					id: 'todoEdit',
					iconCls: 'user',
                    html: 'Here comes the creation form fot todos'
		});
		
	
		Ext.create("Ext.tab.Panel", {
            fullscreen: true,
            tabBarPosition: 'bottom',
			id: 'mainPanel',
			items : [
				listpanel,editpanel
			]
		});
    }   
});
